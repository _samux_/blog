---
title: About me
subtitle: Who am I and what i do
comments: false
author: samu
---

My name is Samuele Tonon i am one of the gazillion IT engineers out there

If you need me, you can contact me:samu (at) samux.org


**Blog content disclaimer**

All of the content and information in this blog post is, to the best of the author’s knowledge, accurate.

Unless otherwise stated, all the contents of the Blog, EXCEPT FOR COMMENTS, constitute the opinion of their respective owners, and them alone; they do not represent the views and opinions of their respective owner’s employers, supervisors, nor do they represent the view of organizations, businesses or institutions their respective owners is a part of.

The content of this Blog is not intended to cause harm, but if You have any concerns about the contents of this Blog, please contact the Author.

Disagreeing with the content of the Blog does not constitute sufficient ground for You to ask the Author to remove or modify any parts of this Blog.

**Copyright policy**

All the text, images and other content being part of this Blog is property of their respective owners, unless noted otherwise.

All logos and trademarks are property of their respective owners. You are not allowed to reproduce, sell, and modify any part of this Blog without Author consent

You are welcome to link to this blog, and to discuss its contents in a respectful manner. When You quote or link to this Blog, please include the Blog’s name in your link.



