---
title: Serverless at home
author: samu
type: post
date: 2019-06-06T21:04:36+00:00
categories:
  - Computer
tags:
  - python
  - k8s
  - faas
---
As serverless is becoming more and more an interesting architecture to look at, I did some small experiments at home and these are the end results.

I am assuming you have at least an Ubuntu 18.04 installed.

you can install microk8s through snap 
<pre class="lang:sh decode:true">
sudo snap install microk8s --classic
</pre>
And i strongly suggest adding the following alias 
<pre class="lang:sh decode:true">
sudo snap alias microk8s.kubectl kubectl
</pre>
This command should return _yes_
<pre class="lang:sh decode:true">
kubectl auth can-i create pods 
</pre>
We create then the namespace
<pre class="lang:sh decode:true">
kubectl create ns kubeless
</pre>
and install **kubeless**, the kubernetes solution for FaaS based on kubernetes. next coming commands are straight from their documentation
pages available [here][1]
setting the release
<pre class="lang:sh decode:true">
export RELEASE=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
</pre>
creating the initial configuration
<pre class="lang:sh decode:true">
kubectl create -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless-$RELEASE.yaml
</pre>
_additional bash magic_ to determine your os 
<pre class="lang:sh decode:true">
export OS=$(uname -s| tr '[:upper:]' '[:lower:]')
</pre>
downloading and installing kubeless in your system under /usr/local/bin
<pre class="lang:sh decode:true">
curl -OL https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless_$OS-amd64.zip 
unzip kubeless_$OS-amd64.zip
sudo mv bundles/kubeless\_$OS-amd64/kubeless /usr/local/bin/
</pre>

Before launching your first sample you need to create a config for kubeless.  **--flatten=true** will create a configuration that include certification authority so you can safely connect without having to add insecure-tls option
<pre class="lang:sh decode:true">
kubectl config view --flatten=true >>~/.kube/config
</pre>

Now let's create a sample python file and let's call it *test2.py*
<pre class="lang:python decode:true " >
def echoing_commands(event, context):
    print (event)
    return event['data']
</pre>

you can now deploy your **amazing** function
<pre class="lang:sh decode:true">
kubeless function deploy hello2 --runtime python3.6 --from-file test2.py --handler test2.echoing_commands
</pre>

Wait for it to be ready:
<pre class="lang:sh decode:true">
kubeless function ls hello2
NAME  	NAMESPACE	HANDLER               	RUNTIME  	DEPENDENCIES	STATUS   
hello2	default  	test2.echoing_commands	python3.6	            	1/1 READY
</pre>

and then you can test it with
<pre class="lang:sh decode:true">
kubeless function call hello2 --data "I am working" 
I am working
</pre>

and that's it, you have a serverless infrastructure at home!
Now you just need some reasons to use it.

[1]: https://kubeless.io/docs/quick-start/#installation
