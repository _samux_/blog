---
title: Open FaaS on microk8s
author: samu
type: post
date: 2019-06-01T12:04:36+00:00
categories:
  - Computer
tags:
  - python
  - k8s
  - faas
  - openfaas
---

Openfaas is an open source framework to provide serverless in a private cloud or simply at home.

As the full Kubernetes could be overkill for your home machine, it is easier and faster to install a lighter version, such as microk8s.

Microk8s is a minimal version of kubernetes available on Ubuntu Linux through snap packages.
with a simple command such as 

<pre class="lang:sh decode:true " >
sudo snap install microk8s --classic
</pre>

you already have installed kubernetes on your machine.

I suggest also add this alias for commodity
<pre class="lang:sh decode:true " >
sudo snap alias microk8s.kubectl kubectl
</pre>

and also then enable dns and dashboard services to ensure you can monitor what is happening
<pre class="lang:sh decode:true " >
microk8s.enable dns
microk8s.enable dashboard
</pre>

Also, if this is a fresh installation for you, i strongly recommend to create a default config file with this command
<pre class="lang:sh decode:true " >
kubectl config view --flatten=true >>~/.kube/config
</pre>

**OpenFaaS**

The following set of commands are taken directly from minikube guid from OpenFaaS documentagion guide, and they have
been adapated to work on microk8s as well.

Microk8s version used for this script is **v1.14.2** on ubuntu 18.04, but it should work for other version as well.

To install openfaas we firs need to set install helm and setup tiller.
This can be easily done with this 3 commands:
<pre class="lang:sh decode:true " >
sudo snap install helm
kubectl -n kube-system create sa tiller
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
</pre>

We need then to initialize helm, apply openfaas namespace, add openfaas repo and then update helm repositories:
<pre class="lang:sh decode:true " >
helm init --skip-refresh --upgrade --service-account tiller
kubectl apply -f https://raw.githubusercontent.com/openfaas/faas-netes/master/namespaces.yml
helm repo add openfaas https://openfaas.github.io/faas-netes/
helm repo update
</pre>

this is a trick to create a random 12 characters password and add it to the environment.
This password will be used to create the authentication mechanism to use openfaas
<pre class="lang:sh decode:true " >
export PASSWORD=$(head -c 12 /dev/urandom | shasum| cut -d' ' -f1)
</pre>

With this command we create a basic authentication mechanism from the password below
<pre class="lang:sh decode:true " >
kubectl -n openfaas create secret generic basic-auth --from-literal=basic-auth-user=admin --from-literal=basic-auth-password="$PASSWORD"
</pre>

We can then proceed to install openfaas through helm
<pre class="lang:sh decode:true " >
helm upgrade openfaas --install openfaas/openfaas --namespace openfaas --set functionNamespace=openfaas-fn --set basic_auth=true
</pre>

If all went well, through this command you should see all services are up and running  through this command:
<pre class="lang:sh decode:true " >
kubectl --namespace=openfaas get deployments -l "release=openfaas, app=openfaas"
</pre>
This is the expected output 
<pre class="lang:sh decode:true " >
NAME                READY   UP-TO-DATE   AVAILABLE   AGE
alertmanager        1/1     1            1           2m
basic-auth-plugin   1/1     1            1           2m
faas-idler          1/1     1            1           2m
gateway             1/1     1            1           2m
nats                1/1     1            1           2m
prometheus          1/1     1            1           2m
queue-worker        1/1     1            1           2m
</pre>

It is now time to setup login for faas-cli. Faas-cli can be easily installed by the usual **unsafe** command in pipe with sudo and a shell. Of course you were not going to check the script anyway right ? 
<pre class="lang:sh decode:true " >
curl -sL https://cli.openfaas.com | sudo sh
</pre>

Now you can enable the login through the command line interface by doing 
<pre class="lang:sh decode:true " >
echo -n $PASSWORD | faas-cli login -g http://$IP:31112 -u admin --password-stdin
</pre>
with $IP as the "external ip" (doesn't work with localhost) of the machine where you installed microk8s / faas 

That's it. if you want you can also access the interface through a web browser by going to http://$IP:31112/ and typing admin:$PASSWORD as your credentials.
From there you can also go and search for some functions to deploy straight in your fresh newly created cluster

