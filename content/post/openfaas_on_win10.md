---
title: Open FaaS on windows 10 with minishift
author: samu
type: post
date: 2019-06-14T21:22:36+00:00
categories:
  - Computer
tags:
  - minishift
  - faas
  - openfaas
---

Previously we have seen how to to install OpenFaas on [microk8s]({{< ref "openfaas_on_microk8s.md" >}}) . Now let's see how to do the same 
on a windows 10 installation.

I am assuming you followed documentation from openshift on how to install minishift on windows 10; in short:
- download minishift.exe and add it somewhere to your %PATH%

- install Hyper V, from *admin* powershell by running:

<pre class="lang:sh decode:true " >
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
</pre>

- after reboot, add a [virtual switch][1] for each network adapter you want to use: if you have a laptop and want to switch from ethernet to wifi, add both.

- add yourself to the Hyper-V Administrator group by running this command from an *admin* powershell:

<pre class="lang:sh decode:true " >
([adsi]"WinNT://./Hyper-V Administrators,group").Add("WinNT://$env:UserDomain/$env:Username,user")
</pre>

- logout, login 

- you can now permanently set which virtual switch minishift is using with:
<pre class="lang:sh decode:true " >
minishift config set hyperv-virtual-switch "External (Wireless)"
</pre>

- or set it up on the fly by launching minishift start with some optiions:
<pre class="lang:sh decode:true " >
minishift start --hyperv-virtual-switch "External (Wireless)"
</pre>
_The name of the virtual switch is case sensitive._

- Minishift is now starting, soon you should get some output like this:
<pre>
...
   OpenShift server started.
   The server is accessible via web console at:
       https://192.168.99.128:8443
\
   You are logged in as:
       User:     developer
       Password: developer
\
   To login as administrator:
       oc login -u system:admin
</pre>

- You can access the web console and verify if the installation is successfull

- add oc binary to your path by running
<pre class="lang:sh decode:true " >
minishift oc-env
</pre>

- Now it is time to install openfaas
<pre class="lang:sh decode:true " >
oc login -u system:admin
\\
oc adm new-project openfaas
oc adm new-project openfaas-fn
</pre>

...to be continued


[1]: https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/connect-to-network
