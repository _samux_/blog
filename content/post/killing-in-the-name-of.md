---
title: Killing in the name of
author: samu
type: post
date: 2013-07-12T15:13:59+00:00
categories:
  - Computer
tags:
  - kill
  - processes
  - python

---
Using psmisc in python i&#8217;ve found this function quite useful in many projects i&#8217;ve been working lately.
  
Basically it will kill all the process older than one hour that match &#8220;name&#8221;

All those try except are just to avoid permission denied on kill or on name check.

<pre class="lang:python decode:true " >def killing_in_the_name_of(name):
    now = time.time()
    for p in psutil.get_process_list():
        exe = ""
        try:
            exe = p.exe
        except Exception,e:
            pass
        if name in exe:
            exetime = now - p.create_time
            if exetime &gt; 3600.0:
                try:
                    p.kill()
                    print "killed %s ( %d s of execution time)"%(exe,exetime)
                except Exception,e:
                    print e
</pre>
