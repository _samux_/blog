---
title: CSP and the sad status of web applications
author: samu
type: post
date: 2019-03-17T10:04:36+00:00
categories:
  - Computer
tags:
  - csp
  - html
  - web
---
As the web is evoling more and more, new features are implemented to make it more secure and prevent most common attacks.

One of the features implemented, that is still trying to become default on all websites, is Content security Policies or, in short CSP.

For more information on CSP, you can take a look at [wikipedia][1] and this [mozilla page][2] .

Implementing CSP on your website is rather easy; understanding what you are
doing, what is being blocked and how to fix it, is not easy at all.

Firefox and Chrome only inform you which policy is blocking a specific content:
css, frame, script; it is then up to you understand your pages and look exactly
what is being blocked; This is not easy at all.

To add on top of this mOst of free software available out doesn't implement best practices to allow
strong CSP policies; therefore you end up setting up really relaxed policies
because the 3rd party tools you are using were made inserting CSS styles inline
web pages or allowing inline javascript code.

What is more alarming is that some of the teams / companies, after being
informed their software is being blocked by some CSP, their solution is to ask
you to relax you rules.

Hopefully there will be some improvements on this: CSP can help you prevent
most of XSS attacks, therefore I do not know of any good reason of not
implement them properly and request vendors - open source or not - to fix and
adapt their software to such good practices

 [1]: https://en.wikipedia.org/wiki/Content_Security_Policy
 [2]: https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
