---
title: A Wonderful world with ansible
author: samu
type: post
date: 2013-08-08T14:50:59+00:00
categories:
  - Computer
  - Linux
tags:
  - ansible

---
Ansible is a beautiful tool to help you manage a lot of linux server.

It follows the philosophy that orchestrating servers should be dead simple rather than complex as writing software.

Here some quick and simple use for ansible 

update apt cache and install python-requests: 

<pre class="lang:sh decode:true " >ansible all -u myuser -s -m apt -a "pkg=python-requests state=installed update_cache=yes"</pre>

copy /tmp/myfile to any host :

<pre class="lang:sh decode:true " >ansible all -u myuser -s -m copy -a "src=/tmp/myfile dest=/tmp/"</pre>

Of course things can be made a bit more complex depending your usage
