---
title: How to use SSH behind a corporate Proxy
type: post
author: samu
date: 2016-11-14T22:10:21+00:00
categores: 
  - Computer
tags:
  - ssh
  - corkscrew
  - cntlm
---

Due to the increase need of security on many companies, it becomes more complex to do normal tasks; tasks that you were used to do
on your normal routines.

One of these is to be able to reach some of your hosts, outside the company network, through ssh, due to some corporate proxy.

We are not really speaking of accessing unauthorized hosts, more than being able to make your tools work with all the software
your company is going to use to facilitate their security and optimize their bandwith consumption.

So i am assuming your company network is using a NTLM proxy to access the outside and, under linux, you are using cntlm, and that is already configured.

Supposing cntlm is configured to listen on also on loopback and on port 3128, you will need to install corkscrew.

Then on **~/.ssh/config** you might want to add this piece of configuration, in this example it is to access github:

<pre class="lang:sh decode:true">
Host github.com
    ProxyCommand corkscrew 127.0.0.1 3128 %h %p
</pre>

and that should be all.
