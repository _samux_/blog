---
title: A suitable vimrc for python
author: samu
type: post
date: 2013-03-11T21:04:36+00:00
categories:
  - Computer
tags:
  - configuration
  - python
  - vim
---
Since i find myself in a lot of trouble when going to a new linux system due to misconfigured vim i thought
  
i might share with you my tipical default configuration for vim that is good for developing in python

here are the main lines, since i use a dark background i like a lot the **&#8220;background=dark&#8221;** feature 

<pre class="lang:vim decode:true " >set compatible
syntax on
set background=dark
if has("autocmd")
  au BufReadPost * if line("'\"") &gt; 0 && line("'\"") &lt; = line("$")
    \| exe "normal g'\"" | endif
endif

set showcmd             
set showmatch           
set incsearch           

autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab " Python
autocmd FileType make set tabstop=8|set shiftwidth=8|set noexpandtab " Makefile
autocmd FileType man set tabstop=8|set shiftwidth=8|set noexpandtab " Man page (also used by psql to edit or view)
autocmd FileType calendar set tabstop=8|set shiftwidth=8|set noexpandtab

</pre>

I hope this will help you too
