---
title: Deobfuscator, decoder for POST urls
author: samu
type: post
date: 2014-09-10T15:56:48+00:00
categories:
  - Computer

---
While looking in your apache or nginx logs, you could end up finding some entries of attempt to hack your machine.

some of these are easily to spot:

`POST %63%67%69%2D%62%69%6E/%70%68%70?%2D%64+%61%6C%6C%6F%77%5F%75%72%6C%5F%69%6E%63%6C%75%64%65%3D%6F%6E+%2D%64+%73%61%66%65%5F%6D%6F%64%65%3D%6F%66%66+%2D%64+%73%75%68%6F%73%69%6E%2E%73%69%6D%75%6C%61%74%69%6F%6E%3D%6F%6E+%2D%64+%64%69%73%61%62%6C%65%5F%66%75%6E%63%74%69%6F%6E%73%3D%22%22+%2D%64+%6F%70%65%6E%5F%62%61%73%65%64%69%72%3D%6E%6F%6E%65+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%64+%63%67%69%2E%66%6F%72%63%65%5F%72%65%64%69%72%65%63%74%3D%30+%2D%64+%63%67%69%2E%72%65%64%69%72%65%63%74%5F%73%74%61%74%75%73%5F%65%6E%76%3D%30+%2D%64+%61%75%74%6F%5F%70%72%65%70%65%6E%64%5F%66%69%6C%65%3D%70%68%70%3A%2F%2F%69%6E%70%75%74+%2D%6E`

but a bit hard to decode.
  
To avoid the pain of looking char by char i&#8217;m sharing a really easy python routine to decode them

<pre class="lang:python decode:true " >import binascii
def parsemi(stri):
    codes=stri.split('%')
    stt=""
    for cc in codes:
        if len(cc) &gt; 2:
            stt+=binascii.unhexlify(cc[:2])
            stt+=cc[2]
        else:
            stt+=binascii.unhexlify(cc)
    print stt
</pre>

just put that string on a variable and passe it to parsemi as an argument and you&#8217;ll get:
  
_cgi-bin/php?-d+allow\_url\_include=on+-d+safe\_mode=off+-d+suhosin.simulation=on+-d+disable\_functions=&#8221;"+-d+open\_basedir=none+-d+auto\_prepend\_file=php://input+-d+cgi.force\_redirect=0+-d+cgi.redirect\_status\_env=0+-d+auto\_prepend\_file=php://input+-n_
