---
title: From Screen to Tmux
author: samu
type: post
date: 2013-07-16T13:20:50+00:00
categories:
  - Computer
tags:
  - screen
  - sysadmin
  - tmux
---


If you ever worked on long task you&#8217;ll end up having the need of detaching from your terminal and come back the next day, without loosing your output.
  

  
This is where **screen** and **tmux** come handy, by making you able to detach your console and logging out.

Lately i&#8217;ve been trying to migrate from screen to tmux, mostly because tmux looks more advanced and modern from screen.

Thanks to [this][1] page it&#8217;s quite easy to migrate from screen to tmux

here&#8217;s a quick reference for you all.

<table>
  <tr>
    <td colspan="3">
      <p>
        The formatting here is simple enough to understand (I would hope). ^ means ctrl+, so ^x is ctrl+x. M- means meta (generally left-alt or escape)+, so M-x is left-alt+x
      </p>
      
      <p>
        It should be noted that this is no where near a full feature-set of either group. This &#8211; being a cheat-sheet &#8211; is just to point out the most very basic features to get you on the road.
      </p>
    </td>
  </tr>
  
  <tr>
    <td>
      <strong>Action</strong>
    </td>
    
    <td>
      <strong>tmux</strong>
    </td>
    
    <td>
      <strong>screen</strong>
    </td>
  </tr>
  
  <tr>
    <td>
      start a new session
    </td>
    
    <td>
      tmux <em>OR</em><br />tmux new <em>OR</em><br />tmux new-session
    </td>
    
    <td>
      screen
    </td>
  </tr>
  
  <tr>
    <td>
      re-attach a detached session
    </td>
    
    <td>
      tmux attach <em>OR</em><br />tmux attach-session
    </td>
    
    <td>
      screen -r
    </td>
  </tr>
  
  <tr>
    <td>
      re-attach an attached session (detaching it from elsewhere)
    </td>
    
    <td>
      tmux attach -d <em>OR</em><br />tmux attach-session -d
    </td>
    
    <td>
      screen -dr
    </td>
  </tr>
  
  <tr>
    <td>
      re-attach an attached session (keeping it attached elsewhere)
    </td>
    
    <td>
      tmux attach <em>OR</em><br />tmux attach-session
    </td>
    
    <td>
      screen -x
    </td>
  </tr>
  
  <tr>
    <td>
      detach from currently attached session
    </td>
    
    <td>
      ^b d <em>OR</em><br />^b :detach
    </td>
    
    <td>
      ^a ^d <em>OR</em><br />^a :detach
    </td>
  </tr>
  
  <tr>
    <td>
      rename-window to newname
    </td>
    
    <td>
      ^b , <newname> <em>OR</em><br />^b :rename-window <newname>
    </td>
    
    <td>
      ^a A <newname>
    </td>
  </tr>
  
  <tr>
    <td>
      list windows
    </td>
    
    <td>
      ^b w
    </td>
    
    <td>
      ^a w
    </td>
  </tr>
  
  <tr>
    <td>
      list windows in chooseable menu
    </td>
    
    <td>
    </td>
    
    <td>
      ^a &#8220;
    </td>
  </tr>
  
  <tr>
    <td>
      go to window #
    </td>
    
    <td>
      ^b #
    </td>
    
    <td>
      ^a #
    </td>
  </tr>
  
  <tr>
    <td>
      go to last-active window
    </td>
    
    <td>
      ^b l
    </td>
    
    <td>
      ^a l
    </td>
  </tr>
  
  <tr>
    <td>
      go to next window
    </td>
    
    <td>
      ^b n
    </td>
    
    <td>
      ^a n
    </td>
  </tr>
  
  <tr>
    <td>
      go to previous window
    </td>
    
    <td>
      ^b p
    </td>
    
    <td>
      ^a p
    </td>
  </tr>
  
  <tr>
    <td>
      see keybindings
    </td>
    
    <td>
      ^b ?
    </td>
    
    <td>
      ^a ?
    </td>
  </tr>
  
  <tr>
    <td>
      list sessions
    </td>
    
    <td>
      ^b s <em>OR</em><br />tmux ls <em>OR</em><br />tmux list-sessions
    </td>
    
    <td>
      screen -ls
    </td>
  </tr>
  
  <tr>
    <td>
      toggle visual bell
    </td>
    
    <td>
    </td>
    
    <td>
      ^a ^g
    </td>
  </tr>
  
  <tr>
    <td>
      create another shell
    </td>
    
    <td>
      ^b c
    </td>
    
    <td>
      ^a c
    </td>
  </tr>
  
  <tr>
    <td>
      exit current shell
    </td>
    
    <td>
      ^d
    </td>
    
    <td>
      ^d
    </td>
  </tr>
  
  <tr>
    <td>
      split pane horizontally
    </td>
    
    <td>
      ^b &#8220;
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      split pane vertically
    </td>
    
    <td>
      ^b %
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      switch to another pane
    </td>
    
    <td>
      ^b o
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      kill the current pane
    </td>
    
    <td>
      ^b x <em>OR</em> (logout/^D)
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      close other panes except the current one
    </td>
    
    <td>
      ^b !
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      swap location of panes
    </td>
    
    <td>
      ^b ^o
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      show time
    </td>
    
    <td>
      ^b t
    </td>
    
    <td>
    </td>
  </tr>
  
  <tr>
    <td>
      show numeric values of panes
    </td>
    
    <td>
      ^b q
    </td>
    
    <td>
    </td>
  </tr>
</table>

 [1]: http://www.dayid.org/os/notes/tm.html
